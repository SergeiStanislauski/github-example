main used libraries
 - butter knife
 - retrofit
 - rxjava, rxandroid
 - constraint
 - dagger2, dagger2 android


The whole project is divided into logical modules. the main of which
1. dagger - is responsible for creating objects
Dagger 2 is dependency injection framework. It is based on the Java Specification Request (JSR) 330. It uses code generation and is based on annotations. The generated code is very relatively easy to read and debug.
Dagger 2 uses the following annotations:
@Module and @Provides: define classes and methods which provide dependencies
@Inject: request dependencies. Can be used on a constructor, a field, or a method
@Component: enable selected modules and used for performing dependency injection
Dagger 2 uses generated code to access the fields and not reflection. Therefore it is not allowed to use private fields for field injection.

In the application there are three types scope:
Singleton, ActivityScope and FragmentScope. Which indicate the life cycle objects.

2. retrofit - responsible for requests for backend
Retrofit is a REST Client for Android and Java by Square. It makes it relatively easy to retrieve and upload JSON (or other structured data) via a REST based webservice.
In Retrofit you configure which converter is used for the dataserialization. Typically for JSON you use GSon, but you can add custom converters to process XML or other protocols.
Retrofit uses the OkHttp library for HTTP requests.

We have two requests:
    @GET("/users/{username}/repos")
    @GET("/orgs/{orgname}/repos")

Request done with help rxjava. (Reactive Extensions for the JVM – a library for composing asynchronous and event-based programs using observable sequences for the Java VM.
We use for internet requests in conjunction with retrofit library)


3. activity - contains all the activites

4. fragment - contains all the fragments

5. data - is responsible for the work with data
getUserRepositories - return user repozitories
getOrganizationsRepositories - return оrganizations repositories


6. utils - helper classes



