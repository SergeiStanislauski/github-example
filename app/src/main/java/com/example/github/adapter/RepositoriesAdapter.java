package com.example.github.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.github.R;
import com.example.github.retrofit.model.Repository;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by evansgelist on 3/26/18.
 */

public class RepositoriesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<Repository> mItems = new ArrayList<>();
    private LayoutInflater mLayoutInflater;

    public RepositoriesAdapter(Context context) {
        this.mContext = context;
        this.mLayoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolderItem(mLayoutInflater.inflate(R.layout.item_repository, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolderItem holderItem = (ViewHolderItem) holder;
        holderItem.bindView(mItems.get(position));
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void swap(List<Repository> items) {
        mItems.clear();
        if (items != null) {
            mItems.addAll(items);
            notifyDataSetChanged();
        }
    }

    public void clear() {
        mItems.clear();
        notifyDataSetChanged();
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        @BindView(R.id.name_text)
        TextView nameText;

        @BindView(R.id.full_name_text)
        TextView fullNameText;

        @BindView(R.id.description_text)
        TextView descriptionText;

        @BindView(R.id.private_text)
        TextView privateText;

        @BindView(R.id.fork_text)
        TextView forkText;

        @BindView(R.id.url_text)
        TextView urlText;

        ViewHolderItem(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        void bindView(Repository repository) {
            nameText.setText(mContext.getString(R.string.name, repository.getName()));
            fullNameText.setText(mContext.getString(R.string.full_name, repository.getFull_name()));
            descriptionText.setText(mContext.getString(R.string.description, repository.getDescription()));
            privateText.setText(mContext.getString(R.string.private_state, Boolean.toString(repository.isPrivateState())));
            forkText.setText(mContext.getString(R.string.fork, Boolean.toString(repository.isFork())));
            urlText.setText(mContext.getString(R.string.html_url, repository.getHtml_url()));
        }
    }
}
