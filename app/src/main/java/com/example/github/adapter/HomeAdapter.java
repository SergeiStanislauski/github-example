package com.example.github.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.github.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by evansgelist on 3/26/18.
 */

public class HomeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<String> mItems = new ArrayList<>();
    private LayoutInflater mLayoutInflater;

    public HomeAdapter(Context context, List<String> list) {
        this.mLayoutInflater = LayoutInflater.from(context);
        this.mItems = list;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolderItem(mLayoutInflater.inflate(R.layout.item_home_action, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolderItem holderItem = (ViewHolderItem) holder;
        holderItem.bindView(mItems.get(position));
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {

        @BindView(R.id.title_text)
        TextView titleText;

        ViewHolderItem(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        void bindView(String str) {
            titleText.setText(str);
        }
    }
}
