package com.example.github.activity;

import android.os.Bundle;

import com.example.github.R;
import com.example.github.activity.base.DaggerActivity;
import com.example.github.fragment.HomeFragment;
import com.example.github.utils.FragmentHelper;

import javax.inject.Inject;

import static com.example.github.fragment.HomeFragment.newInstance;

public class MainActivity extends DaggerActivity {

    @Inject
    FragmentHelper mFragmentHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        openHomeFragment();
    }

    private void openHomeFragment() {
        HomeFragment homeFragment = (HomeFragment) mFragmentHelper.find(getFragmentManager(), HomeFragment.TAG);
        if (homeFragment == null) {
            mFragmentHelper.add(getFragmentManager(), newInstance(), R.id.main_fragment_container, false, false, HomeFragment.TAG);
        }
    }
}
