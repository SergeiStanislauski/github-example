package com.example.github.retrofit.model;

import io.realm.RealmObject;

/**
 * Created by evansgelist on 3/26/18.
 */

public class Repository extends RealmObject {
    private long id;
    private String name;
    private String full_name;
    private String description;
    private boolean privateState;
    private boolean fork;
    private String html_url;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getFull_name() {
        return full_name;
    }

    public String getDescription() {
        return description;
    }

    public boolean isPrivateState() {
        return privateState;
    }

    public boolean isFork() {
        return fork;
    }

    public String getHtml_url() {
        return html_url;
    }
}



