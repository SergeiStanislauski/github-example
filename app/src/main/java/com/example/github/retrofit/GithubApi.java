package com.example.github.retrofit;

import com.example.github.retrofit.model.Repository;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface GithubApi {

    @GET("/users/{username}/repos")
    Single<List<Repository>> getUserRepositories(@Path("username") String searchUser);

    @GET("/orgs/{orgname}/repos")
    Single<List<Repository>> getOrganizationRepositories(@Path("orgname") String searchOrg);
}
