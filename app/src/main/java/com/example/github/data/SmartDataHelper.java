package com.example.github.data;

import com.example.github.retrofit.GithubApi;
import com.example.github.retrofit.model.Repository;
import com.example.github.utils.RxUtils;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.observers.DisposableSingleObserver;


/**
 * Created by evansgelist on 3/27/18.
 */

public class SmartDataHelper {

    @Inject
    RxUtils mRxUtils;

    @Inject
    GithubApi mGithubService;

    @Inject
    public SmartDataHelper() {
    }

    public void getUserRepositories(DisposableSingleObserver<List<Repository>> observer, String searchText) {
        mGithubService.getUserRepositories(searchText)
                .compose(mRxUtils.applySchedulers3())
                .subscribe(observer);
    }

    public void getOrganizationsRepositories(DisposableSingleObserver<List<Repository>> observer, String searchText) {
        mGithubService.getOrganizationRepositories(searchText)
                .compose(mRxUtils.applySchedulers3())
                .subscribe(observer);
    }
}
