package com.example.github;

import android.app.Activity;
import android.app.Application;

import com.example.github.dagger.components.DaggerAppComponent;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by evansgelist on 3/27/18.
 */

public class GithubApp extends Application implements HasActivityInjector {

    private static GithubApp mInstance;
    @Inject
    DispatchingAndroidInjector<Activity> activityInjector;

    public static GithubApp getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        DaggerAppComponent.builder().create(this).inject(this);
        setUpRealm();
    }

    private void setUpRealm() {
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);
    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return activityInjector;
    }
}
