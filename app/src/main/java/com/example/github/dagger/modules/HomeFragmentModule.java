package com.example.github.dagger.modules;

import android.content.Context;

import com.example.github.R;
import com.example.github.adapter.HomeAdapter;
import com.example.github.dagger.scopes.FragmentScope;

import java.util.ArrayList;
import java.util.Arrays;

import dagger.Module;
import dagger.Provides;

/**
 * Created by evansgelist on 3/27/18.
 */

@Module
public class HomeFragmentModule {

    @Provides
    @FragmentScope
    HomeAdapter repositoriesAdapter(Context context) {
        return new HomeAdapter(context,
                new ArrayList<>(Arrays.asList(context.getResources().getStringArray(R.array.string_array_actions))));
    }
}