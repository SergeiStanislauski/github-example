package com.example.github.dagger.scopes;

import java.lang.annotation.Retention;

import javax.inject.Scope;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by evansgelist on 1/30/18.
 */

@Scope
@Retention(RUNTIME)
public @interface FragmentScope {

}
