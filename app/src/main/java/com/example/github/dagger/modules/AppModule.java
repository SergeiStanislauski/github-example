package com.example.github.dagger.modules;

import android.app.Application;

import com.example.github.GithubApp;
import com.example.github.R;
import com.example.github.activity.MainActivity;
import com.example.github.dagger.scopes.ActivityScope;
import com.example.github.retrofit.GithubApi;
import com.example.github.retrofit.GithubService;
import com.example.github.utils.FragmentHelper;
import com.example.github.utils.KeyboardUtil;
import com.example.github.utils.RxUtils;
import com.example.github.utils.SmartErrorHandler;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.android.AndroidInjectionModule;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by evansgelist on 3/26/18.
 */

@Module(includes = AndroidInjectionModule.class)
public abstract class AppModule {

    @Provides
    @Singleton
    static FragmentHelper fragmentHelper() {
        return new FragmentHelper();
    }

    @Provides
    @Singleton
    static KeyboardUtil keyboardUtil() {
        return new KeyboardUtil();
    }

    @Provides
    @Singleton
    static RxUtils rxUtils() {
        return new RxUtils();
    }

    @Provides
    @Singleton
    static SmartErrorHandler smartErrorHandler() {
        return new SmartErrorHandler();
    }

    @Provides
    @Singleton
    static GithubApi githubApi(Application app) {
        return GithubService.createGithubService(app.getResources().getString(R.string.github_oauth_token));
    }

    @Binds
    @Singleton
    abstract Application application(GithubApp app);

    @ActivityScope
    @ContributesAndroidInjector(modules = {MainActivityModule.class})
    abstract MainActivity mainActivityInjector();
}
