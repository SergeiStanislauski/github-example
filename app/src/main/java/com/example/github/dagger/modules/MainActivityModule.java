package com.example.github.dagger.modules;

import android.content.Context;

import com.example.github.activity.MainActivity;
import com.example.github.dagger.scopes.ActivityScope;
import com.example.github.dagger.scopes.FragmentScope;
import com.example.github.fragment.HomeFragment;
import com.example.github.fragment.RepositoriesFragment;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;
import io.reactivex.disposables.CompositeDisposable;
import io.realm.Realm;

/**
 * Created by evansgelist on 3/26/18.
 */

@Module
abstract class MainActivityModule {

    @Provides
    static Realm realm() {
        return Realm.getDefaultInstance();
    }

    @Provides
    static CompositeDisposable compositeDisposable() {
        return new CompositeDisposable();
    }

    @Binds
    @ActivityScope
    abstract Context activityContext(MainActivity activity);

    @FragmentScope
    @ContributesAndroidInjector(modules = {RepositoriesFragmentModule.class})
    abstract RepositoriesFragment repositoriesFragment();

    @FragmentScope
    @ContributesAndroidInjector(modules = {HomeFragmentModule.class})
    abstract HomeFragment homeFragment();
}