package com.example.github.dagger.modules;

import android.content.Context;

import com.example.github.adapter.RepositoriesAdapter;
import com.example.github.dagger.scopes.FragmentScope;

import dagger.Module;
import dagger.Provides;

/**
 * Created by evansgelist on 3/26/18.
 */

@Module
public class RepositoriesFragmentModule {

    @Provides
    @FragmentScope
    RepositoriesAdapter repositoriesAdapter(Context context) {
        return new RepositoriesAdapter(context);
    }
}