package com.example.github.dagger.components;


import com.example.github.GithubApp;
import com.example.github.dagger.modules.AppModule;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjector;

/**
 * Created by evansgelist on 3/26/18.
 */

@Singleton
@Component(modules = AppModule.class)
interface AppComponent extends AndroidInjector<GithubApp> {

    @Component.Builder
    abstract class Builder extends AndroidInjector.Builder<GithubApp> {
    }
}
