package com.example.github.utils;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;

/**
 * Created by evansgelist on 3/26/18.
 */

public class FragmentHelper {

    public FragmentHelper() {
    }

    public Fragment find(FragmentManager fragmentManager, String tag) {
        return fragmentManager.findFragmentByTag(tag);
    }

    public void add(FragmentManager fragmentManager, Fragment fragment, int container, boolean addToBackStack, boolean anim, String tag) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if (anim) {
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        }
        fragmentTransaction.add(container, fragment, tag);
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.commit();
    }
}
