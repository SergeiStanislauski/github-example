package com.example.github.utils;

import com.example.github.GithubApp;
import com.example.github.R;
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;

import static android.widget.Toast.LENGTH_SHORT;
import static android.widget.Toast.makeText;

/**
 * Created by evansgelist on 3/27/18.
 */

public class SmartErrorHandler {

    private final int ERROR_CODE = 404;

    public SmartErrorHandler() {
    }

    public void showError(Throwable throwable) {
        if (throwable instanceof HttpException
                && ((HttpException) throwable).code() == ERROR_CODE) {
            makeText(GithubApp.getInstance(), R.string.no_data, LENGTH_SHORT).show();
        } else {
            makeText(GithubApp.getInstance(), R.string.error_while_searching, LENGTH_SHORT).show();
        }
    }
}
