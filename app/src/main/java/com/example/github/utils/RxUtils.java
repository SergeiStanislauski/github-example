package com.example.github.utils;

import io.reactivex.CompletableTransformer;
import io.reactivex.FlowableTransformer;
import io.reactivex.ObservableTransformer;
import io.reactivex.SingleTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by evansgelist on 2/23/18.
 */

public class RxUtils {

    private final ObservableTransformer schedulersTransformer = observable -> observable.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread());
    private final FlowableTransformer schedulersTransformer2 = observable -> observable.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread());
    private final SingleTransformer schedulersTransformer3 = observable -> observable.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread());
    private final CompletableTransformer schedulersTransformer4 = observable -> observable.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread());

    public RxUtils() {
    }

    public <T> ObservableTransformer<T, T> applySchedulers() {
        return (ObservableTransformer<T, T>) schedulersTransformer;
    }

    public <T> FlowableTransformer<T, T> applySchedulers2() {
        return (FlowableTransformer<T, T>) schedulersTransformer2;
    }

    public <T> SingleTransformer<T, T> applySchedulers3() {
        return (SingleTransformer<T, T>) schedulersTransformer3;
    }

    public <T> CompletableTransformer applySchedulers4() {
        return schedulersTransformer4;
    }
}
