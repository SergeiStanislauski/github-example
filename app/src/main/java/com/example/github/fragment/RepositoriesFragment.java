package com.example.github.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;

import com.example.github.GithubApp;
import com.example.github.R;
import com.example.github.adapter.RepositoriesAdapter;
import com.example.github.data.SmartDataHelper;
import com.example.github.fragment.base.RxRealmButterKnifeDagger2Fragment;
import com.example.github.retrofit.model.Repository;
import com.example.github.utils.KeyboardUtil;
import com.example.github.utils.RxUtils;
import com.example.github.utils.SmartErrorHandler;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.jakewharton.rxbinding2.widget.TextViewTextChangeEvent;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.DisposableSingleObserver;

import static android.text.TextUtils.isEmpty;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static android.widget.GridLayout.VERTICAL;
import static android.widget.Toast.LENGTH_SHORT;
import static android.widget.Toast.makeText;
import static com.example.github.fragment.HomeFragment.TYPE_ORGANIZATION;
import static com.example.github.fragment.HomeFragment.TYPE_REPO;
import static com.example.github.fragment.HomeFragment.TYPE_USER;

public class RepositoriesFragment extends RxRealmButterKnifeDagger2Fragment {

    public static final String TAG = RepositoriesFragment.class.getSimpleName();
    @BindView(R.id.search_edit_text)
    EditText mSearchEditText;

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.progress_bar)
    ContentLoadingProgressBar mProgressBar;

    @Inject
    SmartDataHelper mSmartDataHelper;

    @Inject
    RepositoriesAdapter mAdapter;

    @Inject
    SmartErrorHandler mSmartErrorHandler;

    @Inject
    RxUtils mRxUtils;

    @Inject
    KeyboardUtil mKeyboardUtil;

    private DisposableSingleObserver<List<Repository>> mRequestObserver;
    private int mType;

    public static RepositoriesFragment newInstance(int type) {
        RepositoriesFragment fragment = new RepositoriesFragment();
        Bundle args = new Bundle();
        args.putInt(TYPE_REPO, type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getViewId() {
        return R.layout.repositories_fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            mType = bundle.getInt(TYPE_REPO, -1);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindRecyclerView();
        bindSearchEdit();
        mKeyboardUtil.showKeyboard(getActivity());
    }

    private void bindSearchEdit() {
        mSearchEditText.setHint(getHintText(mType));
        RxTextView.textChangeEvents(mSearchEditText)
                .debounce(300, TimeUnit.MILLISECONDS)
                .doOnError(throwable -> mAdapter.clear())
                .compose(mRxUtils.applySchedulers())
                .subscribeWith(getSearchObserver());
    }

    private void bindRecyclerView() {
        mAdapter = new RepositoriesAdapter(getActivity());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), VERTICAL));
    }

    public void makeSearchRequest(String searchText) {
        if (!getDisposable().isDisposed()
                && mRequestObserver != null) {
            getDisposable().remove(mRequestObserver);
        }
        lock(true);
        mRequestObserver = getRequestObserver();
        switch (mType) {
            case TYPE_USER:
                mSmartDataHelper.getUserRepositories(mRequestObserver, searchText);
                break;

            case TYPE_ORGANIZATION:
                mSmartDataHelper.getOrganizationsRepositories(mRequestObserver, searchText);
                break;
        }
        getDisposable().add(mRequestObserver);
    }

    private DisposableObserver<TextViewTextChangeEvent> getSearchObserver() {
        return new DisposableObserver<TextViewTextChangeEvent>() {
            @Override
            public void onComplete() {
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onNext(TextViewTextChangeEvent onTextChangeEvent) {
                if (isAdded()) {
                    if (isEmpty(onTextChangeEvent.text())) {
                        mAdapter.clear();
                    } else {
                        makeSearchRequest(onTextChangeEvent.text().toString());
                    }
                }
            }
        };
    }

    private DisposableSingleObserver<List<Repository>> getRequestObserver() {
        return new DisposableSingleObserver<List<Repository>>() {
            @Override
            public void onSuccess(List<Repository> list) {
                if (isAdded()) {
                    lock(false);
                    mAdapter.swap(list);
                    if (list.size() == 0) {
                        makeText(GithubApp.getInstance(), R.string.no_data, LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onError(Throwable e) {
                if (isAdded()) {
                    lock(false);
                    mAdapter.clear();
                    mSmartErrorHandler.showError(e);
                }
                e.printStackTrace();
            }
        };
    }

    private void lock(boolean state) {
        mRecyclerView.setVisibility(state ? GONE : VISIBLE);
        if (state) {
            mProgressBar.show();
        } else {
            mProgressBar.hide();
        }
    }

    private int getHintText(int type) {
        switch (type) {
            case TYPE_USER:
                return R.string.input_user_name;

            case TYPE_ORGANIZATION:
                return R.string.input_organization_name;
        }
        throw new RuntimeException("wrong type");
    }
}

