package com.example.github.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.github.R;
import com.example.github.adapter.HomeAdapter;
import com.example.github.fragment.base.RxRealmButterKnifeDagger2Fragment;
import com.example.github.utils.FragmentHelper;
import com.example.github.utils.RecyclerItemClickListener;

import javax.inject.Inject;

import butterknife.BindView;

import static android.widget.GridLayout.VERTICAL;

/**
 * Created by evansgelist on 3/26/18.
 */

public class HomeFragment extends RxRealmButterKnifeDagger2Fragment {

    public static final String TAG = HomeFragment.class.getSimpleName();
    public static final String TYPE_REPO = "TYPE_REPO";
    public static final int TYPE_USER = 111;
    public static final int TYPE_ORGANIZATION = 222;

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    @Inject
    HomeAdapter mAdapter;

    @Inject
    FragmentHelper mFragmentHelper;

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public int getViewId() {
        return R.layout.home_fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindView();
    }

    private void bindView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), VERTICAL));
        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(),
                mRecyclerView,
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        switch (position) {
                            case 0:
                                openRepositoriesFragment(TYPE_USER);
                                break;

                            case 1:
                                openRepositoriesFragment(TYPE_ORGANIZATION);
                                break;
                        }
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                    }
                })
        );
    }

    private void openRepositoriesFragment(int type) {
        mFragmentHelper.add(getFragmentManager(), RepositoriesFragment.newInstance(type), R.id.main_fragment_container, true, true, RepositoriesFragment.TAG);
    }
}
